class Board
  attr_accessor :grid, :ships
  symbols = [ x: "fired", s: "damaged ship", nil: "empty"]

  def initialize(grid= nil)
    @grid = grid || Board.default_grid
    @ships = 0
  end

  def [](pos)
    x, y = pos
    @grid[x][y]
  end

  def []=(pos, val)
    x, y = pos
    @grid[x][y] = val
  end

  def self.default_grid
    Array.new(10) {Array.new(10)}
  end

  def count
    @grid.flatten.count(:s)
  end

  def empty?(pos= nil)
    if pos
      self[pos].nil?
    else
      self.count==0
    end
  end

  def full?
    @grid.flatten.none? {|spot| spot == nil}
  end

  def place_random_ship
    raise "Board is full" if self.full?
    if self.empty?
      until self.full?
        pos = self.randomz
        self[pos] = :s if self.empty?(pos)
      end
    end
  end

  def randomz
    [rand(@grid.length),rand(@grid.length)]
  end

  def won?
    @grid.flatten.none? {|spot| spot == :s}
  end


end
