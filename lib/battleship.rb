class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board = Board.new)
    @board = board
    @player = player

  end



  def attack(pos)
    @board[pos] = :x
  end

  def count
    board.count
  end

  def game_over?
    board.won?
  end

  def play_turn
    move = player.get_play
    self.attack(move)
  end

end
